import { render, fireEvent, screen } from "@testing-library/react";
import HealthDeclaration from "./HealthDeclatation";

describe('HealthDeclaration Component', () => {
    beforeAll(() => {
        // remove local storage item
        window.localStorage.removeItem('healthDeclarationId');
    });

    test('renders health declaration form', () => {
        // Render the component on virtual dom
        render(<HealthDeclaration />);
        
        // Assertions
        // the button should be on the document
        expect(screen.getByRole('button')).toBeInTheDocument();
    });

    test('renders "You have submitted successfully" if the submit button was clicked', async () => {
        // Render the component on virtual dom
        render(<HealthDeclaration />)

        // Act (fill in the form and click button)
        fireEvent.change(screen.getByLabelText(/name/i), {
            target: {value: 'SOLA'},
        });
        fireEvent.change(screen.getByLabelText(/temperature/i), {
            target: {value: '36.6'},
        });
        fireEvent.change(screen.getByLabelText(/symptoms/i), {
            target: {value: false},
        });
        fireEvent.change(screen.getByLabelText(/contact/i), {
            target: {value: false},
        });

        // form submit
        fireEvent.click(screen.getByText(/submit/i));

        // Assertions
        const alert = await screen.findByRole('alert')
        expect(alert).toHaveTextContent(/you have submitted successfully/i);
        expect(window.localStorage.getItem('healthDeclarationId')).toBeTruthy();
    });
});