import { Fragment, useEffect, useState } from "react";
import styles from './HealthDeclatation.module.css';

const symptomsCheckText = 'Do you have any of the following symptoms now or within the last 14 days: Cough, smell/taste impairment, fever, breathing difficulties, body aches, headaches, fatigue, sore throat, diarrhoea, and / or running nose (even if your symptoms are mild)?';

const contactCheckText = 'Have you been in contact with anyone who is suspected to have or/has been diagonosed with Covid-19 within the last 14 days?';

const HealthDeclaration = () => {
    const [isSubmitted, setIsSubmitted] = useState(false);
    const [name, setName] = useState('');
    const [temperature, setTemperature] = useState('');
    const [symptomsCheck, setSymptomsCheck] = useState(false);
    const [contactCheck, setContactCheck] = useState(false);
    const [error, setError] = useState('');

    // when the component is executed, check the stored isSubmitted info
    useEffect(() => {
        const storedHealthDeclarationId = localStorage.getItem('healthDeclarationId');
        if (storedHealthDeclarationId) {
            // get data from firebase
            fetch(`${process.env.REACT_APP_FIREBASE_DATABASE}/${storedHealthDeclarationId}.json`)
            .then(res => res.json())
            .then(data => {
               setIsSubmitted(true);
               setName(data.name);
               setTemperature(data.temperature);
               setSymptomsCheck(data.symptomsCheck);
               setContactCheck(data.contactCheck);
            });
        }
      }, []);

    const symptomsCheckChangeHandler = () => {
        setSymptomsCheck(!symptomsCheck);
    }

    const setContactCheckChangeHandler = () => {
        setContactCheck(!contactCheck);
    }

    const submitHandler = (event) => {
        event.preventDefault();

        const data = {
            name,
            temperature,
            symptomsCheck,
            contactCheck
        }

        // save data
        fetch(`${process.env.REACT_APP_FIREBASE_DATABASE}.json`, {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
              'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(data => {
            // saved to firebase success
            if (data.name) {
                setIsSubmitted(true);
                localStorage.setItem('healthDeclarationId', data.name);
            }
            else {
                setError('Failed to submit the health covid-19 declaration!');
            }
        })
        .catch(e => {
          setError(e.message ?? 'Failed to submit the health covid-19 declaration!');
        });
    }

    return (
       <Fragment>
        {!isSubmitted && (
            <form onSubmit={submitHandler} className={styles.form}>
                <div className={styles.control}>
                    <label>
                        Name
                        <input
                            name="name"
                            value={name}
                            onChange={e => setName(e.target.value)} 
                            type='text'
                        />
                    </label>
                </div>
                <div className={styles.control}>
                    <label>
                        Temperature
                        <input
                            name="temperature"
                            value={temperature}
                            onChange={e => setTemperature(e.target.value)} 
                            type='text'
                        />
                    </label>
                </div>
                <div className={styles.control}>
                    <label>
                        <input
                            name="symptomsCheck"
                            onChange={symptomsCheckChangeHandler} 
                            checked={symptomsCheck}
                            type='checkbox'
                        />
                        {symptomsCheckText}
                    </label>
                </div>
                <div className={styles.control}>
                    <label>
                        <input
                            name="contactCheck"
                            onChange={setContactCheckChangeHandler} 
                            checked={contactCheck}
                            type='checkbox'
                        />
                        {contactCheckText}
                    </label>
                </div>
                <div className={styles.actions}>
                    <button type="submit">Submit</button>
                    {error && <p className={styles.error}>{error}</p>}
                </div>
            </form>
        )}
        {isSubmitted && (
            <div className={styles.submitted}>
                <p className={styles['submitted-title']} role="alert">You have submitted successfully!</p>
                <table>
                    <tbody>
                        <tr className={styles['table-row']}>
                            <th>Name</th>
                            <td>{name}</td>
                        </tr>
                        <tr className={styles['table-row']}>
                            <th>Temperature</th>
                        <td>{temperature}</td>
                        </tr>
                        <tr className={styles['table-row']}>
                            <th>{symptomsCheckText}</th>
                            <td>{symptomsCheck ? 'YES' : 'NO'}</td>
                        </tr>
                        <tr className={styles['table-row']}>
                            <th>{contactCheckText}</th>
                            <td>{contactCheck ? 'YES' : 'NO'}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        )}
       </Fragment>
    )
}

export default HealthDeclaration;